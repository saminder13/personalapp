
import argparse


DEFAULT_FORMAT = '{0:40s}{1}{2:>11}'

"""
  Simple Interest Equation (Principal + Interest)
A = P(1 + rt)

Where:

A = Total Accrued Amount (principal + interest)
P = Principal Amount
I = Interest Amount
r = Rate of Interest per year in decimal; r = R/100
R = Rate of Interest per year as a percent; R = r * 100
t = Time Period involved in months or years

"""


def calculate_simple_interest(principal, rate, timeperiod):
    """
    return total accrued amount and interest earned
    :param principal 
    :param rate in percentage
    :timeperiod time period
    :return:
    """
    r = rate / 100
    total_accrued = principal*(1 + (r*timeperiod))

    print((DEFAULT_FORMAT.format("Total accrued amount will be", ':', total_accrued)))
    print((DEFAULT_FORMAT.format("Interest Earned will be", ':', total_accrued - principal)))


parser = argparse.ArgumentParser(description='Usage:  python calculate_simple_interest.py --principal 1000 '
                                             '--rate 7 --timeperiod 2')
parser.add_argument('--principal', type=float, help='principal ammount', required=True)
parser.add_argument('--rate', type=float, help='rate of interest in percentage per annum', required=True)
parser.add_argument('--timeperiod', type=float, help = 'time period in years')

args = parser.parse_args()

calculate_simple_interest(args.principal, args.rate, args.timeperiod)
