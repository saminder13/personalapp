from unittest import TestCase
from unittest.mock import patch
from utilities.get_stock_sell_evaluation import get_stock_sell_evaluation

DEFAULT_FORMAT = '{0:40s}{1}{2:>11}\n'

class TestStockSellEvaluation(TestCase):

    @patch('builtins.print')
    def test_get_stock_sell_evaluation(self, mock_print):
        # Set up test data
        bought_price = 10.0
        sold_price = 15.0
        number_of_shares = 50
        investment_money = None
        commission_fees = True

        # Call the function
        get_stock_sell_evaluation(bought_price, sold_price, number_of_shares, investment_money, commission_fees)

        # Extract the string values from the tuples in mock_print.call_args_list
        printed_values = [call[0][0] for call in mock_print.call_args_list]

        # Assert that the expected numeric values are present in the printed values with tolerance
        self.assertAlmostEqual(bought_price*number_of_shares, float(printed_values[0].split(":")[-1].strip()), places=2)
        self.assertAlmostEqual(50.0, float(printed_values[1].split(":")[-1].strip()), places=2)
        self.assertAlmostEqual((number_of_shares * (sold_price - bought_price) - 20), float(printed_values[2].split(":")[-1].strip()), places=2)
