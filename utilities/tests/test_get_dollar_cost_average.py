import unittest
from unittest.mock import patch
from utilities.get_dollar_cost_average import get_dollar_cost_average

class TestGetDollarCostAverage(unittest.TestCase):

    @patch('builtins.print')
    def test_get_dollar_cost_average(self, mock_print):
        # Set up test data
        existing_stocks = 100
        existing_avg_cost = 10.0
        current_stock_price = 15.0
        new_investment_money = 500.0

        # Call the function
        get_dollar_cost_average(existing_stocks, existing_avg_cost, current_stock_price, new_investment_money)

        # Extract the string values from the tuples in mock_print.call_args_list
        printed_values = [call[0][0] for call in mock_print.call_args_list]

        # Assert that the expected numeric values are present in the printed values with tolerance
        self.assertAlmostEqual(133.33333333333334, float(printed_values[0].split(":")[-1].strip()), places=2)
        self.assertAlmostEqual(11.25, float(printed_values[1].split(":")[-1].strip()), places=2)
        self.assertAlmostEqual(50.0, float(printed_values[2].split(":")[-1].strip()), places=2)
        self.assertAlmostEqual(33.33333333333333, float(printed_values[3].split(":")[-1].strip()), places=2)





if __name__ == '__main__':
    unittest.main()
