from unittest import TestCase
from unittest.mock import patch
from utilities.get_expected_sell_price import get_expected_sell_price

DEFAULT_FORMAT = '{0:40s}{1}{2:>11}\n'

class TestStockSellEvaluation(TestCase):


    @patch('builtins.print')
    def test_get_expected_sell_price(self, mock_print):
        # Set up test data
        bought_price = 10.0
        profit_amount = 500
        number_of_shares = 100

        # Call the function
        get_expected_sell_price(number_of_shares, bought_price, profit_amount)

        # Extract the string values from the tuples in mock_print.call_args_list
        printed_values = [call[0][0] for call in mock_print.call_args_list]

        # Assert that the expected numeric values are present in the printed values with tolerance
        self.assertAlmostEqual(15, float(printed_values[0].split(":")[-1].strip()), places=2)