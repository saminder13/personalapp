import unittest
from unittest.mock import patch
from utilities.get_ai_dca_required_money import get_ai_dca_required_money
from sympy import Symbol, solve

class TestGetAIDCARequiredMoney(unittest.TestCase):

    DEFAULT_FORMAT = '{0:40s}{1}{2:>11}\n'

    @patch('builtins.print')
    def test_get_ai_dca_required_money_invalid_inputs(self, mock_print):
        # Set up test data
        existing_stocks = 100
        existing_avg_cost = 10.0
        current_stock_price = 15.0
        new_investment_money = 500.0

        # Call the function
        get_ai_dca_required_money(existing_stocks, existing_avg_cost, current_stock_price, new_investment_money)

        # Assert that the expected print calls were made
        expected_print_calls = [
            mock_print.call(self.DEFAULT_FORMAT.format("Total new shares to buy", ": ", "66.66")),
            mock_print.call(self.DEFAULT_FORMAT.format("Money Required", ": ", "1000.00"))
        ]

        # Extract the actual calls from mock_print
        actual_print_calls = [call[0] for call in mock_print.call_args_list]

        # Assert that the expected values are present in the actual calls
        for expected_call in expected_print_calls:
            self.assertIn(expected_call, actual_print_calls)

    @patch('builtins.print')
    def test_get_ai_dca_required_money_invalid_inputs(self, mock_print):
        # Set up test data with invalid inputs (negative money_required)
        existing_stocks = 100
        existing_avg_cost = 10.0
        current_stock_price = 5.0  # Lower the price to make money_required negative
        new_desired_avg_price = 12.0

        # Call the function
        result = get_ai_dca_required_money(existing_stocks, existing_avg_cost, current_stock_price, new_desired_avg_price)

        # Assert that an error message is printed
        mock_print.assert_called_with("wrong inputs logically can not be determined")

        # Assert that the function returns an error dictionary
        self.assertEqual(result, {"error": "wrong inputs logically can not be determined"})

if __name__ == '__main__':
    unittest.main()
