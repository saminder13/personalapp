"""
This module should return the expected sell price trigger for given profit or loss
"""

import argparse
from sympy.solvers import solve
from sympy import Symbol


DEFAULT_FORMAT = '{0:40s}{1}{2:>11}\n'


"""
i/p = number of shares
bp = bought price per share
profit_price = price to make profit
loss_price = price to sell the stock


return sell_price


profit = sell_price*number_of_shares - bp*number_of_shares

sell_price = (profit + bp*number_of_shares) // number_of_shares


unitTest
bp = 10
number_of_shares = 1000
profit = 100
sell_price
"""


def get_expected_sell_price(number_of_shares, bought_price, profit_amount):

    sell_price = (profit_amount + (bought_price*number_of_shares)) / number_of_shares
    print((DEFAULT_FORMAT.format("SELL PRICE SHOULD BE", ": ", sell_price)))
    return sell_price

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Usage:  python get_expected_sell_price --stocks 1000 --boughtprice 10 --profit 1000')
    parser.add_argument('--stocks', type=int, help='existing number of stocks')
    parser.add_argument('--boughtprice', type=float, help = 'per share price bought', required=True)
    parser.add_argument('--profit', type=float, help='destired profit or loss', required=True )

    args = parser.parse_args()

    get_expected_sell_price(args.stocks, args.boughtprice, args.profit)


