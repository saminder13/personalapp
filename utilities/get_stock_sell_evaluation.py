
import argparse


DEFAULT_FORMAT = '{0:40s}{1}{2:>11}\n'


def get_stock_sell_evaluation(bought_price, sold_price, number_of_shares=None, investment_money=None, commission_fees=True):
    """ This method is to analyse the get_stock_sell_evaluation

    :param investment_money: money which can be invested in the market
    :param bought_price: price stock bought at
    :param sold_price:  price stocks sold at
    :param number_of_shares: number of shares
    :param: commission_fees:  Boolean that commission will be deducted from trade
    :return: percentage profit or loss until today.
    """

    if not number_of_shares:
        number_of_shares = investment_money / bought_price
    percentage_loss_or_gain = ((sold_price - bought_price)/bought_price) * 100
    money_diff = (number_of_shares * (sold_price - bought_price))

    #taking commission into consideration
    money_diff = money_diff-20 if commission_fees else money_diff

    print((DEFAULT_FORMAT.format("Money invested ", ':', number_of_shares*bought_price)))
    print((DEFAULT_FORMAT.format("%age gain or loss", ':', percentage_loss_or_gain)))
    print((DEFAULT_FORMAT.format("Money gained or lost", ':', money_diff)))
    
    return


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Usage:  python get_stock_price_diff.py --stocks 275 '
                                                 '--boughtPrice 9.58 --soldPrice 11.77')
    parser.add_argument('--investmentMoney', type=float, help='amount of money for investment')
    parser.add_argument('--stocks', type=float, help='amount of money for investment')
    parser.add_argument('--boughtPrice', type=float, help = 'per share price stock bought at', required=True)
    parser.add_argument('--soldPrice', type=float, help='per share price stock sold at', required=True )

    args = parser.parse_args()

    if not (args.investmentMoney or args.stocks):
        raise Exception("Please provide either investmentMoney or stocks argument")

    get_stock_sell_evaluation(args.boughtPrice, args.soldPrice, number_of_shares=args.stocks,investment_money=args.investmentMoney)

