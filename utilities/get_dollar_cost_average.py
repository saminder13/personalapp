
import argparse


DEFAULT_FORMAT = '{0:40s}{1}{2:>11}\n'


def get_dollar_cost_average(existing_stocks, existing_avg_cost, current_stock_price, new_investment_money):
    """ TODO: Make this method documentation correct.
    This method is to analyse the target for the investment money

    :param investment_money: money which can be invested in the market
    :param bought_price: price stock bought at
    :param sold_price:  price stocks sold at
    :param number_of_shares: number of shares
    :param: commission_fees:  Boolean that commission will be deducted from trade
    :return: percentage profit or loss until today.
    """
	
    
    total_existing_book_cost = existing_stocks * existing_avg_cost

    new_shares_to_buy = new_investment_money / current_stock_price

    total_shares = existing_stocks + new_shares_to_buy


    new_avg_share_cost = (total_existing_book_cost + new_investment_money) / total_shares


    previous_percentage_loss_or_gain = ((existing_stocks*(current_stock_price - existing_avg_cost))/total_existing_book_cost)*100
    percentage_loss_or_gain = ((total_shares*(current_stock_price - new_avg_share_cost))/ (total_existing_book_cost+new_investment_money)) * 100

    #taking commission into consideration
    #money_diff = money_diff-20 if commission_fees else money_diff

    print((DEFAULT_FORMAT.format("Total number of shares", ': ', total_shares)))
    print((DEFAULT_FORMAT.format("New avg share cost", ': ', new_avg_share_cost)))
    print((DEFAULT_FORMAT.format("Previous overall %age gain or loss", ': ', previous_percentage_loss_or_gain)))
    print((DEFAULT_FORMAT.format("New overall %age gain or loss", ': ', percentage_loss_or_gain)))
    return


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Usage:  python get_stock_normalization.py --existingStocks 1000 '
                                                 '--existingAvgCost 9 --currentStockPrice 5 --newInvestmentMoney 10000')
    parser.add_argument('--existingStocks', type=int, help='existing number of stocks')
    parser.add_argument('--existingAvgCost', type=float, help='existing price per share average-cost')
    parser.add_argument('--currentStockPrice', type=float, help = 'per share price stock currently trading at', required=True)
    parser.add_argument('--newInvestmentMoney', type=float, help='New money trying to invest to repurchase the shares', required=True )

    args = parser.parse_args()

    get_dollar_cost_average(args.existingStocks, args.existingAvgCost, args.currentStockPrice, args.newInvestmentMoney)

