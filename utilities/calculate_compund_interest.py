
import argparse


DEFAULT_FORMAT = '{0:40s}{1}{2:>21}'

"""
  Compound Interest Equation (Principal + Interest)
A = P (1 + r/n) power (nt)

Where:

A = Total Accrued Amount (principal + interest)
P = Principal Amount
r = Rate of Interest per year in percentage; r = R/100
n = the number of times that interest is compounded per year
t = Time Period involved in months or years

If an amount of $5,000 is deposited into a savings account at an annual interest rate of 5%, compounded monthly, 
the value of the investment after 10 years can be calculated as follows...

P = 5000. r = 5/100 = 0.05 (decimal). n = 12. t = 10.

"""


def calculate_simple_interest(principal, rate, compound_number, timeperiod):
    """
    return total accrued amount and interest earned
    :param principal 
    :param rate in percentage
    :param compound_number: number of times that interest is compounded per year
    :timeperiod time period
    :return:
    """
    r = rate / 100
    total_accrued = principal*(pow((1 + (r / compound_number)), (compound_number*timeperiod)))

    print((DEFAULT_FORMAT.format("Total accrued amount will be", ':', total_accrued)))
    print((DEFAULT_FORMAT.format("Interest Earned will be", ':', total_accrued - principal)))


parser = argparse.ArgumentParser(description='Usage:  python calculate_simple_interest.py --principal 1000 '
                                             '--rate 7 --timeperiod 2')
parser.add_argument('--principal', type=float, help='principal ammount', required=True)
parser.add_argument('--rate', type=float, help='rate of interest in percentage per annum', required=True)
parser.add_argument('--compound_number', type=float, help='number of times that interest is compounded per year')
parser.add_argument('--timeperiod', type=float, help = 'time period in years')

args = parser.parse_args()

calculate_simple_interest(args.principal, args.rate, args.compound_number, args.timeperiod)
