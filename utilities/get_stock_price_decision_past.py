
import argparse


DEFAULT_FORMAT = '{0:40s}{1}{2:>11}'


def get_stock_price_decision_past(stocks, bought_price, sold_price, current_price):
    """ This method is to analyse if the decision taken in the past while selling the stock
    was successful or not compared to today price

    :param stocks: number of stocks
    :param bought_price: price stock bought at
    :param sold_price:  price stocks sold at
    :param current_price:  current price of stocks
    :return: percentage profit or loss until today.
    """

    percentage_loss_or_gain = ((sold_price - current_price)/bought_price) * 100
    money_diff = (stocks*(sold_price-current_price))
    print((DEFAULT_FORMAT.format("Price stock bought", ':', bought_price)))
    print((DEFAULT_FORMAT.format("Price stock sold", ':', sold_price)))
    print((DEFAULT_FORMAT.format("Current stock price: ", ':', current_price)))
    print((DEFAULT_FORMAT.format("%age gain or loss for one share", ':', percentage_loss_or_gain)))
    print((DEFAULT_FORMAT.format("Money gained or lost for the trade ", ':', money_diff)))
    return


parser = argparse.ArgumentParser(description='Usage: python get_stock_price_diff.py --stocks=44 --boughtPrice=12 '
                                             '--soldPrice=34 --currentPrice=123')
parser.add_argument('--stocks', type=float, help = 'total number of stocks bought', required=True )
parser.add_argument('--boughtPrice', type=float, help = 'price stocks bought at', required=True)
parser.add_argument('--soldPrice', type=float, help = 'price stocks sold at', required=True )
parser.add_argument('--currentPrice', type=float, help = 'current price of the stocks', required=True)

args = parser.parse_args()

get_stock_price_decision_past(args.stocks, args.boughtPrice, args.soldPrice, args.currentPrice)


