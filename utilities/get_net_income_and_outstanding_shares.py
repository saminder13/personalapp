
import argparse


DEFAULT_FORMAT = '{0:40s}{1}{2:>11}'

"""
  EPS = company_net_income / total_outstanding_shares
    company_net_income == revenue_generated - total_expenses

  P/E ratio = stockPrice / EPS
    P/E means how the stock is getting evaluated in the market today
        (It determines whether it's overvalued or underpriced)
        try to find industry p/e for this.

"""

def get_company_net_income(eps, total_outstandsing_shares):
    company_net_income =  eps*total_outstandsing_shares
    print((DEFAULT_FORMAT.format("Company net income (in Million) ", ':', company_net_income)))


def get_outstanding_shares(mcap, sharePrice, eps):
    """
    return outstanding shares of the company
    :param mcap: market capitalization
    :param sharePrice:  current share price
    :return:
    """
    outstanding_shares = mcap / sharePrice

    print((DEFAULT_FORMAT.format("MCAP", ':', mcap)))
    print((DEFAULT_FORMAT.format("Current Share price", ':', sharePrice)))
    print((DEFAULT_FORMAT.format("Total outstanding shares (in Million) ", ':', outstanding_shares)))

    get_company_net_income(eps, outstanding_shares)




parser = argparse.ArgumentParser(description='Usage:  python get_net_income_and_outstanding_shares.py --mcap 726.11 --sharePrice 18.65 --eps 1.37')
parser.add_argument('--mcap', type=float, help='company market capitalization', required=True)
parser.add_argument('--sharePrice', type=float, help='single share price', required=True)
#parser.add_argument('--p/e', type=float, help = 'p/e ratio')
parser.add_argument('--eps', type=float, help='eps ratio')

args = parser.parse_args()

get_outstanding_shares(args.mcap, args.sharePrice ,args.eps)
