
import argparse
from sympy import Symbol
from sympy.solvers import solve


DEFAULT_FORMAT = '{0:40s}{1}{2:>11}\n'


def get_ai_dca_required_money(existing_stocks, existing_avg_cost, current_stock_price, new_desired_avg_price):
    """ 
        This method is to analyse the target for the investment money

        :param investment_money: money which can be invested in the market
        :param bought_price: price stock bought at
        :param sold_price:  price stocks sold at
        :param number_of_shares: number of shares
        :param: commission_fees:  Boolean that commission will be deducted from trade
        :return: percentage profit or loss until today.
    """

    x = Symbol('x')

    # Equation representing the desired average cost
    equation = (new_desired_avg_price * existing_stocks + x * new_desired_avg_price - existing_stocks * existing_avg_cost - current_stock_price * x)

    # Solve the equation for x (number of new shares needed)
    shares = solve(equation, x)[0]

    # Calculate the money required to buy the new shares
    money_required = shares * current_stock_price

    if money_required < 0:
        print("wrong inputs logically can not be determined")
        return {
            "error": "wrong inputs logically can not be determined"
        }

    print((DEFAULT_FORMAT.format("Total new shares to buy", ': ', shares)))
    print((DEFAULT_FORMAT.format("Money Required", ': ', money_required)))
    return


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Usage:  python get_ai_avg.py --existingStocks 1000 '
                                                 '--existingAvgCost 9 --currentStockPrice 11 --desiredAvgPrice 5')
    parser.add_argument('--existingStocks', type=int, help='existing number of stocks')
    parser.add_argument('--existingAvgCost', type=float, help='existing price per share average-cost')
    parser.add_argument('--currentStockPrice', type=float, help = 'per share price stock currently trading at', required=True)
    parser.add_argument('--desiredAvgPrice', type=float, help='New desired average price', required=True )

    args = parser.parse_args()

    get_ai_dca_required_money(args.existingStocks, args.existingAvgCost, args.currentStockPrice, args.desiredAvgPrice)
