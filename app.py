import dash
from dash import html, dcc
from dash.dependencies import Input, Output
import subprocess
import logging

# Set up logging
logging.basicConfig(filename='app.log', level=logging.INFO)

app = dash.Dash(__name__)

# Predefined modules for the dropdown
predefined_modules = ['get_ai_dca_required_money', 'get_dollar_cost_average', 'get_expected_sell_price', 'get_stock_sell_evaluation']

app.layout = html.Div([
    html.H1("Utility Script Runner", style={'color': 'navy', 'margin-bottom': '20px'}),
    
    html.Div([
        dcc.Dropdown(
            id='module-dropdown',
            options=[{'label': module, 'value': module} for module in predefined_modules],
            value=predefined_modules[0],
            style={'width': '80%', 'margin-right': '20px', 'color': 'darkslategray'}
        ),
        
        html.Button('Show Module Arguments', id='show-arguments-button', n_clicks=0, style={'background-color': 'steelblue', 'color': 'white'}),
    ], style={'display': 'flex', 'align-items': 'center', 'margin-bottom': '20px'}),
    
    dcc.Input(id='arguments-input', type='text', placeholder='Enter arguments', style={'width': '100%', 'margin-bottom': '20px', 'padding': '10px', 'border': '1px solid lightgray'}),
    
    html.Button('Run module', id='run-button', n_clicks=0, style={'background-color': 'green', 'color': 'white', 'padding': '10px', 'border': 'none', 'cursor': 'pointer'}),
    
    html.Div(id='output-container', style={'margin-top': '20px', 'padding': '10px', 'border': '1px solid lightgray', 'background-color': 'whitesmoke'}),
], style={'max-width': '800px', 'margin': 'auto'})


def get_argparse_help(module_name):
    try:
        # Run the script with a dummy argument to capture the argparse help message
        result = subprocess.check_output(['python', '-m', f"utilities.{module_name}", '--help'])
        return result.decode('utf-8')
    except subprocess.CalledProcessError as e:
        return f"Error: {e.output.decode('utf-8')}"
    except Exception as e:
        return f"Error: {str(e)}"

def run_script(module_name, arguments):
    try:
        result = subprocess.check_output(['python', '-m', f"utilities.{module_name}"] + arguments.split())
        return result.decode('utf-8')
    except subprocess.CalledProcessError as e:
        return f"Error: {e.output.decode('utf-8')}"
    except Exception as e:
        return f"Error: {str(e)}"

@app.callback(
    Output('output-container', 'children'),
    Input('show-arguments-button', 'n_clicks'),
    Input('run-button', 'n_clicks'),
    Input('module-dropdown', 'value'),
    Input('arguments-input', 'value'),  # Pass arguments-input as an input
)
def show_arguments(n_clicks_show, n_clicks_run, module_name, arguments):
    # Log the input values
    logging.info(f"Inputs: n_clicks_show={n_clicks_show}, n_clicks_run={n_clicks_run}, module_name={module_name}, arguments={arguments}")

    # Check which button was clicked
    ctx = dash.callback_context
    triggered_id = ctx.triggered_id.split('.')[0] if ctx.triggered_id else None

    if triggered_id != 'run-button' and n_clicks_show > 0:
        # If "Show Arguments" button was clicked
        help_message = get_argparse_help(module_name)
        return html.Pre(help_message, style={'whiteSpace': 'pre-wrap'})
    elif triggered_id == 'run-button' and n_clicks_run > 0 and arguments:
        # If "Run Script" button was clicked and arguments are provided

        result = run_script(module_name, arguments)

        # Log the result
        logging.info(f"Result: {result}")

        return dcc.Markdown(result, dangerously_allow_html=True)

    # If none of the conditions are met, clear the output
    return ''


if __name__ == '__main__':
    app.run_server(host="0.0.0.0", port=8080, debug=True)
