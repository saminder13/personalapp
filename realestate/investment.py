"""
this module will give the investment returns
### REQUIREMENTS: pip install mortgage
"""


from mortgage import Loan

def get_monthly_mortgage_amount(principal, interest, term):
    """
    #mortgage module: definition: https://mortgage.readthedocs.io/en/latest/api.html#module-mortgage.loan
    :param mortgage: total mortgage amount
    :param interest: total interest percentage
    :param term: total loan term
    :return:  monthly payment
    """
    loan = Loan(principal=principal, interest=interest, term=term)
    return loan.monthly_payment


def get_cash_flow(
        total_house_price=500000,
        condo_fees=0,
        tax=3000,
        down_payment=100000,
        interest_rate_percent=2.9,
        rental_income=1700,
        term=30
):
    """

    :param total_house_price:
    :param condo_fees:
    :param tax:
    :param down_payment:
    :param interest_rate_percent:
    :param rental_income:
    :return:  cash_flow

    """
    mortgage_amount = total_house_price - down_payment
    monthly_mortgage_amount = get_monthly_mortgage_amount(mortgage_amount, interest_rate_percent/100, term)
    total_expenses = monthly_mortgage_amount + condo_fees + tax/12
    cash_flow = rental_income - total_expenses

    print(("TOTAL PROPERTY EXPENSES {}".format(total_expenses)))
    print(("Investment Cash Flow {}".format(cash_flow)))

    return cash_flow



get_cash_flow(total_house_price=600000, condo_fees=0, tax=8200, rental_income=0, down_payment=156000, interest_rate_percent=2.54)
#get_cash_flow(total_house_price=450000, condo_fees=0, tax=3500, rental_income=2000, down_payment=90000, interest_rate_percent=2.9)
#get_cash_flow(total_house_price=500000, condo_fees=0, tax=3700, rental_income=2200, down_payment=100000, interest_rate_percent=2.9)