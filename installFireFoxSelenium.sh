#!/bin/bash

# Install Firefox
install_firefox() {
    if command -v apt-get &>/dev/null; then
        sudo apt-get update
        sudo apt-get install firefox -y
    elif command -v yum &>/dev/null; then
        sudo yum install firefox -y
    else
        echo "Unsupported package manager. Please install Firefox manually."
        exit 1
    fi
}

# Find the installation path of Firefox
firefox_path=$(which firefox)

# Add the Firefox binary location to the PATH
echo "export PATH=\$PATH:$firefox_path" >> ~/.bashrc
source ~/.bashrc

echo "Firefox installed successfully and added to the PATH."
