#! /usr/bin/python

import csv
import operator
import pprint
from yahoo_finance import Share


currentPrice={}
today_change={}
percentage_change={}
days_range={}
avg_volume={}
today_volume={}
dividend_share={}
earnings_share={}
percentage_earnings_growth={}

def findMax(array_of_elements):
    """
    Sort each dictionary data structure element in array
    :param array_of_elements: list of dictionary elements
    :return:
    """

    # TODO: make this logic better instead of storing it in a separate variable; try to create new files with data
    new_array = []
    for element in array_of_elements:
        sorted_data = sorted(list(element.items()), key=operator.itemgetter(1))
        new_array.append(sorted_data)
    print(new_array)
    return new_array

def get_stock_info(stock):
    """
    Call various methods to get stocks info
    :param stock:
    :return:
    """
    stock_symbol = stock[0]
    data = Share(stock_symbol)
    data.refresh()

    # TODO: use OOPS here to get rid of thiscurrentPrice={}
    global today_change
    global percentage_change
    global days_range
    global avg_volume
    global today_volume
    global dividend_share
    global earnings_share
    global percentage_earnings_growth


    # get today's price
    currentPrice.update({stock_symbol: data.get_price()})

    # get stock price change today
    today_change.update({stock_symbol: data.get_change()})

    # find percentage change
    percentage_change.update({stock_symbol: data.get_percent_change()})

    # get today stock price range
    days_range.update({stock_symbol: data.get_days_range()})

    # Avergae volume sell/buy today
    avg_volume.update({stock_symbol: data.get_avg_daily_volume()})

    # volume sell/buy today
    today_volume.update({stock_symbol: data.get_volume()})

    # find dividend share
    dividend_share.update({stock_symbol: data.get_dividend_share()})

    # get earnings share for stock
    earnings_share.update({stock_symbol: data.get_earnings_share()})

    # find ratio of stock price growth earnings
    percentage_earnings_growth.update({stock_symbol: data.get_price_earnings_growth_ratio()})

    #print avg_volume
    #print today_volume

    
def read_stocks_info_from_file(file):
    with open(file, 'rt') as stocks_file:
        stocks = csv.reader(stocks_file)
        for stock in stocks:
            get_stock_info(stock)

if __name__ == "__main__":
    DEFAULT_FILE = 'companylist.csv'

    list_of_stats = [
        currentPrice,
        today_change,
        percentage_change,
        days_range,
        avg_volume,
        today_volume,
        dividend_share,
        earnings_share,
        percentage_earnings_growth,
    ]

    # read_stocks_info_from_file(DEFAULT_FILE)
    read_stocks_info_from_file('sample_file.csv')

    # Call Max function to sort data created by reading stocks above.
    data = findMax(list_of_stats)

    # add logic here to return results here
