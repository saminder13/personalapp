#! /usr/bin/python

import csv
import operator
import os
import pprint
from yahoo_finance import Share


class Stock(object):

    def __init__(self):
        self.currentPrice = {}
        self.today_change = {}
        self.percentage_change = {}
        self.days_range = {}
        self.avg_volume = {}
        self.today_volume = {}
        self.dividend_share = {}
        self.earnings_share = {}
        self.percentage_earnings_growth = {}

    def get_stock_info(self, stock):
        """
        Call various methods to get stocks info
        :param stock:
        :return:
        """
        stock_symbol = stock[0]
        data = Share(stock_symbol)
        data.refresh()

        try:
            # get today's price
            self.currentPrice.update({stock_symbol: data.get_price()})

            # get stock price change today
            self.today_change.update({stock_symbol: data.get_change()})

            # find percentage change
            self.percentage_change.update({stock_symbol: data.get_percent_change()})

            # get today stock price range
            self.days_range.update({stock_symbol: data.get_days_range()})

            # Average volume sell/buy today
            self.avg_volume.update({stock_symbol: data.get_avg_daily_volume()})

            # volume sell/buy today
            self.today_volume.update({stock_symbol: data.get_volume()})

            # find dividend share
            self.dividend_share.update({stock_symbol: data.get_dividend_share()})

            # get earnings share for stock
            self.earnings_share.update({stock_symbol: data.get_earnings_share()})

            # find ratio of stock price growth earnings
            self.percentage_earnings_growth.update({stock_symbol: data.get_price_earnings_growth_ratio()})

            list_of_stats = {
                'currentPrice': self.currentPrice,
                'todayChange': self.today_change,
                'todayPercentageChange': self.percentage_change,
                'daysRange': self.days_range,
                'averageVolume': self.avg_volume,
                'todayVolume': self.today_volume,
                'dividendShare': self.dividend_share,
                'earningsShare': self.earnings_share,
                'percentageEarningsGrowth': self.percentage_earnings_growth,
            }

            self.create_files_for_data(list_of_stats)

        except Exception as e:
            print((e.message))
            pass

    def call_find_max_utility(self):
        list_of_stats = {
            'currentPrice': self.currentPrice,
            'todayChange': self.today_change,
            'todayPercentageChange': self.percentage_change,
            'daysRange': self.days_range,
            'averageVolume': self.avg_volume,
            'todayVolume': self.today_volume,
            'dividendShare': self.dividend_share,
            'earningsShare': self.earnings_share,
            'percentageEarningsGrowth': self.percentage_earnings_growth,
        }

        self.create_files_for_data(list_of_stats)
        return

    def read_stocks_info_from_file(self, file):
        with open(file, 'rt') as stocks_file:
            stocks = csv.reader(stocks_file)
            for stock in stocks:
                self.get_stock_info(stock)

    @staticmethod
    def create_files_for_data(dict_of_elements):
        """
        Sort each dictionary data structure element in dict
        :param dict_of_elements: dict of dictionary elements
        :return:
        """

        cwd = os.getcwd()

        for key, value in list(dict_of_elements.items()):
            file_path = cwd+'\data\%s.txt'% key
            print(('File created at ', file_path))
            try:
                with open(file_path, 'w') as stock_data_file:
                    sorted_data = sorted(list(value.items()), key=operator.itemgetter(1))
                    stock_data_file.write(str(sorted_data)+'\n')
            except Exception as e:
                print((e.message))
                pass

        return None

if __name__ == "__main__":
    #DEFAULT_FILE = 'companylist.csv'
    DEFAULT_FILE = 'sample_file.csv'


    # creating Stock object
    stocks_object = Stock()

    # calling read_stocks_info_from_file(DEFAULT_FILE)
    stocks_object.read_stocks_info_from_file(DEFAULT_FILE)

    # Call Max function to sort data created by reading stocks above.
    stocks_object.call_find_max_utility()

