"""
This file is created to get data for individual stocks and estimate the risk and trigger the order accordingly
"""
import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from utilities import get_pre_evaluation_money
from utilities import get_expected_sell_price

number_of_shares = 687
company = 'TD'
bought_price = 73.76
commission_fees = 20
total_buy_price = commission_fees + (number_of_shares * bought_price)
profit_amount = 101

sold_price_trigger = get_expected_sell_price.get_expected_sell_price(
    number_of_shares,
    bought_price,
    profit_amount + 20     # adding 20 for trade commission fees
)

get_pre_evaluation_money.pre_evaluation(
    bought_price,
    sold_price_trigger,
    number_of_shares=number_of_shares
)


