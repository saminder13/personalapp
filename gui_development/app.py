import dash
from dash import html, dcc
from dash.dependencies import Input, Output
import subprocess
import argparse

app = dash.Dash(__name__)

# Predefined modules for the dropdown
predefined_modules = ['utility_module_1', 'utility_module_2']

app.layout = html.Div([
    html.H1("Utility Script Runner"),
    dcc.Dropdown(
        id='module-dropdown',
        options=[{'label': module, 'value': module} for module in predefined_modules],
        value=predefined_modules[0],  # Default selected module
        style={'width': '50%'}
    ),
    dcc.Input(id='arguments-input', type='text', placeholder='Enter arguments'),
    html.Button('Run Script', id='run-button', n_clicks=0),
    html.Div(id='output-container'),
])

def get_argparse_help(module_name):
    try:
        # Run the script with a dummy argument to capture the argparse help message
        result = subprocess.check_output(['python', 'utilities.py', module_name, '--help'])
        return result.decode('utf-8')
    except subprocess.CalledProcessError as e:
        return f"Error: {e.output.decode('utf-8')}"
    except Exception as e:
        return f"Error: {str(e)}"

@app.callback(
    Output('output-container', 'children'),
    Input('run-button', 'n_clicks'),
    Input('module-dropdown', 'value'),
)
def run_script(n_clicks, module_name):
    if n_clicks > 0:
        help_message = get_argparse_help(module_name)
        return html.Pre(help_message, style={'whiteSpace': 'pre-wrap'})
    return ''

if __name__ == '__main__':
    app.run_server(debug=True)
