from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from dash.testing.application_runners import import_app


# Load the Dash app
app = import_app('app')


# Path to the Firefox binary
# firefox_binary_path = '/path/to/firefox' not required installFireFox already adding
# this to path

def test_dropdown_options():
    # Define your test for dropdown options here
    firefox_options = webdriver.FirefoxOptions()
    firefox_options.binary_location = firefox_binary_path
    
    with webdriver.Firefox() as driver:
        driver.get(app.server_url)
        
        # Wait for dropdown to load
        WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "module-dropdown"))
        )

        dropdown_options = driver.find_elements_by_css_selector("#module-dropdown option")

        # Example: Check if the dropdown has the expected options
        assert len(dropdown_options) == 3
        assert [option.text for option in dropdown_options] == ['get_ai_dca_required_money', 'get_dollar_cost_average', 'get_expected_sell_price']

def test_run_button():
    # Define your test for dropdown options here
    firefox_options = webdriver.FirefoxOptions()
    firefox_options.binary_location = firefox_binary_path

    # Define your test for the "Run Script" button here
    with webdriver.Firefox() as driver:
        driver.get(app.server_url)
        
        # Wait for the "Run Script" button to load
        WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "run-button"))
        )

        driver.find_element_by_id("run-button").click()

        # Wait for the output to appear
        WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "output-container"))
        )

# Add more tests as needed
