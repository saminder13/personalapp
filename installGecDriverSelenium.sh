#!/bin/bash

# Set the version you want to download
GECKODRIVER_VERSION="v0.31.0"

# Define the URL to download the geckodriver archive
GECKODRIVER_URL="https://github.com/mozilla/geckodriver/releases/download/${GECKODRIVER_VERSION}/geckodriver-${GECKODRIVER_VERSION}-linux64.tar.gz"

# Create a temporary directory
temp_dir=$(mktemp -d)

# Navigate to the temporary directory
cd "$temp_dir"

# Download the geckodriver archive
wget "$GECKODRIVER_URL"

# Extract the archive
tar -xzvf "geckodriver-${GECKODRIVER_VERSION}-linux64.tar.gz"

# Move geckodriver to a directory in your PATH (e.g., /usr/local/bin)
sudo mv geckodriver /usr/local/bin/

# Clean up temporary files
rm -rf "$temp_dir"

echo "Geckodriver installed successfully. You can now use it in your Selenium tests."
