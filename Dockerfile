FROM python:3.11.7-alpine3.18
WORKDIR /app
COPY . .
RUN pip install -r requirements.txt
CMD ["python", "app.py"]